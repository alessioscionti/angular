import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  id: string;
  nome: number;
  cognome: number;
  telefono: string;
}


@Component({
  selector: 'app-laraveltest',
  templateUrl: './laraveltest.component.html',
  styleUrls: ['./laraveltest.component.css']
})
export class LaraveltestComponent implements OnInit{

  constructor(private http: HttpClient) {}
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = new MatTableDataSource();
  variabile="sono una variabile di prova";
  variabile2="adesso sono una nuova variabile";
  nuovavariabile="Richiedi dati";
  arrayclienti:any;
  ngOnInit(): void {
    
  }
  async onClick(event : Event){
/*     const headers = new Headers();
    headers.append('XSRF-TOKEN', 'eyJpdiI6ImFIYWZFRTdqRzVVQTVqQ0dab3pETnc9PSIsInZhbHVlIjoibmlXTkovUFlwWXlyV3NYaFpMRm5xUHNFR2RPbGUza24yYldvSnJwUkNMQXByN013Q3p4aW10Q3BhTjBaUS9Mb3NBWU9YbE1LQkdMK1JwSzlhWlFMTStXVFB0MWZLdEJTdXc1MzMrL2hiUENlUkxUbDNhUzM3Rm8wbnREVmV5NXgiLCJtYWMiOiI4NThlY2IzOTExYzU3NGU2OTE4NzdmYmVkYzk5ZGVkMzdmY2EzNzJhNDljMjdkZTU0MDYzZTBmNGQzZTkwMDg3IiwidGFnIjoiIn0%3D; laravel_session=eyJpdiI6IlVENFpMdmxoeVFSQm13TjBlQ2RPTXc9PSIsInZhbHVlIjoiUmhiVlJ1dWIrWDRLNWx3Z1FTTnNVY0F0MnRjRHh6NW5sZGcrdzFMcGphSDlsME8vNElxUG5SQ2grWGRlRjJwVktSdUw1MTVQZDl0d0FJdW1JREdRY0dzQ3hmQXBHM2swMUkwR3FjVldNU2hjNVZTUC9iaUVZK3ZDaUZyUUgyemgiLCJtYWMiOiI0YjA5ODQ4MzA4ZWZiYmE5MDM0ZmRiMWRmZDEwZGIyNzkxNTJkYWE5ZjM0NzdmN2QxZmViNDg3YjZhYmI2ODJjIiwidGFnIjoiIn0%3D');
 */    
    const httpHeaders = new HttpHeaders();
    this.http.get('http://127.0.0.1:8000/api/clienti', { headers: httpHeaders } ).subscribe((response)=>{
    
    this.arrayclienti=response
    console.log(this.arrayclienti);
    const ELEMENT_DATA: PeriodicElement[] = [{id:this.arrayclienti.id,nome:this.arrayclienti.nome,cognome:this.arrayclienti.cognome,telefono:this.arrayclienti.telefono}]
    //this.dataSource = new MatTableDataSource(ELEMENT_DATA);
  })
}

applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource = new MatTableDataSource(this.arrayclienti);
  this.dataSource.filter = filterValue.trim().toLowerCase();
  console.log(filterValue);
  
}
}
