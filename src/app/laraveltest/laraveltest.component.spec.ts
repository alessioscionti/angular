import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LaraveltestComponent } from './laraveltest.component';

describe('LaraveltestComponent', () => {
  let component: LaraveltestComponent;
  let fixture: ComponentFixture<LaraveltestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LaraveltestComponent]
    });
    fixture = TestBed.createComponent(LaraveltestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
